﻿using System;
using System.Threading;

/*
 * 哲学家就餐问题 
 * 五个哲学家做一桌，饿了就吃饭，否则就思考，相邻哲学家之间有一把叉子，只有同时拿起两把叉子才能吃饭，否则就思考
 *
 * 限制能同时拿起叉子的哲学家数量为 max-1，这样，极端情况下，限制状况下能同时那叉子的哲学家都拿起了叉子，桌子上还剩一把，这样就可以让其相邻的一个哲学家
 * 就餐了，但这并不高效，极端情况下，只有一个哲学家可以拿起左右两把叉子，剩下的四个就要等待
 *
 * 哲学家、叉子分别编号
 * 哲学家编号【1-5】
 * 叉子编号【1-5】
 * 实现上，哲学家用一个数组线程来表示，每个哲学家两边的叉子编号分别是： 左边叉子编号跟哲学家编号 数字形式上是一样的，右边的叉子的编号则是左+1
 *
 * 当得到可运行的信号后，先尝试获得左叉子，再尝试获得右叉子，吃饭，然后释放右叉子，左叉子，以及可运行信号
 * 
 */
namespace Philosopher_sln1
{
    class Program
    {
        private static Semaphore phil_sema = new Semaphore(4, 4); // 拿到信号的哲学家运行，最多允许四个哲学家拿到信号
        private static Semaphore[] fork_sema = new Semaphore[5];  // 叉子
        private static Thread[] philosopher = new Thread[5]; // 哲学家，哲学家编号1-5， 每个哲学家左右两边的叉子的编号遵循，左叉子编号等于哲学家编号，右叉子编号等于左叉子编号+1

        // 哲学家工作线程, 参数是哲学家编号
        static void PhilosopherRun1(object ph_id)
        {
            int num = (int)ph_id;

            while (true)
            {
                phil_sema.WaitOne(); // 当前哲学家尝试获得可进餐许可
                fork_sema[num % 5].WaitOne(); // 尝试获得左叉子， 左叉子的编号跟哲学家编号 数字形式上面是相同的数字
                fork_sema[(num + 1) % 5].WaitOne(); // 尝试获得其右边的叉子
                MyEat(num + 1);
                fork_sema[(num + 1) % 5].Release(); // 释放右边的叉子
                fork_sema[num % 5].Release(); // 释放zuo边的叉子
                phil_sema.Release(); // 释放可吃饭资格
                Thread.Sleep(1000);
            }
        }

        static void MyEat(int i)
        {
            Console.WriteLine("{0} 哲学家进餐", i.ToString());
        }
        
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                fork_sema[i] = new Semaphore(1, 1);
            }

            for (int i = 0; i < 5; i++)
            {
                philosopher[i] = new Thread(new ParameterizedThreadStart(PhilosopherRun1));
                philosopher[i].Start(i);
            }

            philosopher[0].Join();
            philosopher[1].Join();
            philosopher[2].Join();
            philosopher[3].Join();
            philosopher[4].Join();
            
            Console.WriteLine("Hello World!");
        }
    }
}