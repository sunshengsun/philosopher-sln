﻿using System;
using System.Threading;

// 哲学家就餐问题 
/*
 * 将哲学家编号的解法
 *
 * 1 - 将哲学家从1-n编号，将哲学家编号作为参数传递给线程方法
 * 2 - 使用信号量表示叉子
 * 3 - 线程方法内 根据 奇偶数 作出先拿left or right 叉子的判断
 * 
 */
namespace philosopher_sln2
{
    class Program
    {
        // 五个信号量（1，1）表示叉子
        private static Semaphore[] fork_sema = new Semaphore[5];
        
        // 五个线程对象 表示 五个哲学家
        private static Thread[] ta = new Thread[5];

        // private static Object obj = new object();
        
        // 线程方法， 参数为哲学家编号｜名字
        static void Run1(object n_id)
        {
            // todo
            // 如果传递的id是奇数，则让该哲学家线程先尝试拿到左叉子
            // 如果传递的id是偶数，则让该哲学家线程先尝试拿到右叉子
            int num = (int) n_id;
            
            while (true)
            {
                if (num % 2 == 1)
                {
                        // 让当前哲学家尝试获得位于他左手边的叉子, 左边的叉子编号跟哲学家编号一致， 右边的叉子是当前哲学家的编号+1
                        fork_sema[num % 5].WaitOne();
                        fork_sema[(num+1)% 5].WaitOne();
                        Myeat(num);
                        fork_sema[(num + 1) % 5].Release();
                        fork_sema[num % 5].Release();
                }
                else if (num % 2 == 0)
                {
                        fork_sema[(num+1) % 5].WaitOne();
                        fork_sema[num % 5].WaitOne();
                        Myeat(num);
                        fork_sema[num % 5].Release();
                        fork_sema[(num + 1) % 5].Release();
                }
                else
                { 
                    MyThinking(num);
                }
                Thread.Sleep(1000);
            }
        }

        static void Myeat(int i)
        {
            Console.WriteLine("{0} 号哲学家吃饭", i + 1);
        }

        static void MyThinking(int i)
        {
            Console.WriteLine("{0} 号哲学家思考", i + 1);
        }
        
        static void Main(string[] args)
        {
            for (int i = 0; i < 5; i++)
            {
                fork_sema[i] = new Semaphore(1, 1);
            }

            for (int i = 1; i <= 5; i++)
            {
                ta[i-1] = new Thread(new ParameterizedThreadStart(Run1));
                ta[i-1].Start(i-1);
            }

            ta[0].Join();
            ta[1].Join();
            ta[2].Join();
            ta[3].Join();
            ta[4].Join();
            
            
            Console.WriteLine("Hello World!");
        }
    }
}